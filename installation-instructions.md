# Installation Instructions
In order to participate and perform the programming exercises of the course,
you need to have the following installed.

## (1) Install SDKMAN
Within a BASH terminal, run the following command

    curl -s "https://get.sdkman.io" | bash

Follow the instructions on-screen to complete installation.

It might complain regarding missing `zip` and `unzip`. You can install
these in Ubuntu with the following command

    sudo apt install zip unzip

Using _GIT for Windows BASH_ it might complain that ZIP is missing.
Download zip and unpack the bin folder to `/usr/bin`
You can read more about various installation options here

* https://sdkman.io/install
* https://medium.com/@gayanper/sdkman-on-windows-661976238042
* https://stackoverflow.com/questions/38782928/how-to-add-man-and-zip-to-git-bash-installation-on-windows
* https://sourceforge.net/projects/gnuwin32/files/zip/3.0/zip-3.0-bin.zip/download


## (2) Install JDK

    sdk ls java
    sdk install 17.0.5-oracle
    java --version

## (3) Install Gradle

    sdk install gradle

## (4) Install JetBrains Intellij IDEA (_Ultimate, 30-days trial_)

* https://www.jetbrains.com/idea/
* https://www.jetbrains.com/help/idea/2022.3/how-to-use-wsl-development-environment-in-product.html
