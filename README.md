# Gradle, 1 day
### 2023 January

# Links
* [Installation Instructions](./installation-instructions.md)

# Usage of this GIT Repo
Ensure you have a [GIT client](https://git-scm.com/downloads) installed,
open a GIT BASH terminal window  and type the commands below to clone this repo.

    mkdir -p ~/gradle-course/my-solutions
    cd ~/gradle-course
    git clone <git https url> gitlab

![GIT HTTPS URL](./img/git-url.png)

During the course, solutions will be push:ed to this repo, and you can get
these by a `git pull` operation

    cd ~/gradle-course/gitlab
    git pull


# Build Solution/Demo Programs
All programs are ordinary Java programs and can be compiled using any appropriate
tool. Several of the demo & solutions programs has a Gradle build file and can
therefore be built by

    gradle build         # BASH


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
