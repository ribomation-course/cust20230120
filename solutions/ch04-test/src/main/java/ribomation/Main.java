package ribomation;

import com.google.gson.GsonBuilder;

import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        var app = new Main();
        app.run();
    }

    private void run() {
        var repo = new AccountRepo();
        repo.load("/accounts.csv");
        repo.getAccounts().stream()
                .limit(10)
                .forEach(System.out::println);

        var list = repo.getAccounts().stream()
                .limit(5)
                .collect(Collectors.toList());
        var json = new GsonBuilder().setPrettyPrinting().create().toJson(list);
        System.out.println(json);
    }
}
