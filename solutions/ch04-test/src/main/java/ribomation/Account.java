package ribomation;

import java.util.StringJoiner;

public class Account {
    //accno;balance;customer;email;city
    private String accno;
    private double balance;
    private String customer;
    private String email;
    private String city;

    public Account() {}

    public Account(String accno, double balance, String customer, String email, String city) {
        this.accno = accno;
        this.balance = balance;
        this.customer = customer;
        this.email = email;
        this.city = city;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Account.class.getSimpleName() + "[", "]")
                .add("accno='" + accno + "'")
                .add("balance=" + balance)
                .add("customer='" + customer + "'")
                .add("email='" + email + "'")
                .add("city='" + city + "'")
                .toString();
    }

    public String getAccno() {
        return accno;
    }

    public double getBalance() {
        return balance;
    }

    public String getCustomer() {
        return customer;
    }

    public String getEmail() {
        return email;
    }

    public String getCity() {
        return city;
    }
}
