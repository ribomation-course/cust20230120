package ribomation;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AccountRepoTest {

    @Test
    @DisplayName("should have two objects")
    void load() {
        var repo = new AccountRepo();
        repo.load("/accounts-small.csv");
        assertEquals(2, repo.getAccounts().size());
    }
}
