package ribomation;

public class Main {
    public static void main(String[] args) {
        var app = new Main();
        app.run();
    }

    private void run() {
        var repo = new AccountRepo();
        repo.load("/accounts.csv");
        repo.getAccounts().stream()
                .limit(10)
                .forEach(System.out::println);
    }
}