package ribomation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AccountRepo {
    private List<Account> accounts = new ArrayList<>();

    public List<Account> getAccounts() {
        return accounts;
    }

    public void load(String resource) {
        var is = getClass().getResourceAsStream(resource);
        var in = new BufferedReader(new InputStreamReader(is));
        try (in) {
            accounts = in.lines()
                    .skip(1)
                    .map(csv -> csv.split(";"))
                    .map(f -> new Account(f[0], Double.parseDouble(f[1]), f[2], f[3], f[4]))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
